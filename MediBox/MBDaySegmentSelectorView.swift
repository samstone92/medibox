//
//  MBDaySegmentSelectorView.swift
//  MediBox
//
//  Created by Sam Stone on 08/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class MBDaySegmentSelectorView :UIView{
  
  @IBOutlet var contentView: UIView!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setup(){
    loadViewFromNib()
    
  }
  
  func loadViewFromNib() {
    let bundle = NSBundle(forClass: self.dynamicType)
    let nib = UINib(nibName: "MBDaySegmentSelector", bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    view.frame = bounds
    view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
    
    self.layer.cornerRadius = view.frame.size.height/2;
    self.clipsToBounds = true
    
    self.layer.borderColor = UIColor.orangeColor().CGColor
    self.layer.borderWidth = 3
    self.addSubview(view);
  }
  
  
}