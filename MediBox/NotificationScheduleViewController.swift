//
//  NotificationScheduleViewController.swift
//  MediBox
//
//  Created by Sam Stone on 02/09/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class NotificationScheduleViewController : UITableViewController, MBTimePickerTextViewProtocol{
  
  
  @IBOutlet var morningPicker: MBTimePickerTextView!
  @IBOutlet var afternoonPicker: MBTimePickerTextView!
  @IBOutlet var eveningPicker: MBTimePickerTextView!
  @IBOutlet var nightPicker: MBTimePickerTextView!
  
  var notificationSchedule : MBNotificationSchedule!
  
  override func viewDidLoad() {
    setupPickers()
    getData()
    self.hideKeyboardWhenTappedAround()
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Ubuntu", size: 25)!]
  }
  
  func setupPickers(){
    morningPicker.timePickerDelegate = self
    morningPicker.tag = DayPartType.Morning.rawValue
    
    afternoonPicker.timePickerDelegate = self
    afternoonPicker.tag = DayPartType.Afternoon.rawValue

    eveningPicker.timePickerDelegate = self
    eveningPicker.tag = DayPartType.Evening.rawValue

    nightPicker.timePickerDelegate = self
    nightPicker.tag = DayPartType.Night.rawValue
  }
  
  override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    let headerView = view as! UITableViewHeaderFooterView
    headerView.textLabel!.text = "What times would you like to be notified about your medication?"
  }
  
  func pickerValueChanged(tag: Int, date:NSDate) {
    notificationSchedule!.reminders[tag] = NSDate().getJSONStringFormat(date)
    updateSchedule()
  }
  
  func updateSchedule(){
    MBNotificationScheduleService.updateSchedule(NSUserDefaults().getCurrentUserEmail(), parameters: notificationSchedule!.toJSON()) { (completion :Bool) in
      if completion{
        print("done")
      }
    }
  }
  
  func getData(){
    MBNotificationScheduleService.getNotificationForUser(NSUserDefaults().getCurrentUserEmail()) { (completion:Bool, schedule:MBNotificationSchedule?) in
      if completion{
        self.notificationSchedule = schedule!
        self.setTimeOnPickers()
      }
    }
  }
  
  func setTimeOnPickers(){
    morningPicker.setDate(NSDate().getDateFromJSONString((notificationSchedule.reminders[DayPartType.Morning.rawValue])))
    afternoonPicker.setDate(NSDate().getDateFromJSONString((notificationSchedule.reminders[DayPartType.Afternoon.rawValue])))
    eveningPicker.setDate(NSDate().getDateFromJSONString((notificationSchedule.reminders[DayPartType.Evening.rawValue])))
    nightPicker.setDate(NSDate().getDateFromJSONString((notificationSchedule.reminders[DayPartType.Night.rawValue])))
  }
  
}
