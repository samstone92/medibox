//
//  DayPartTypes.swift
//  MediBox
//
//  Created by Sam Stone on 10/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation

enum DayPartType: Int, CustomStringConvertible {
  case Morning = 0
  case Afternoon = 1
  case Evening = 2
  case Night = 3
  static var count: Int { return DayPartType.Night.hashValue + 1 }
  
  var description: String {
    switch self {
      case .Morning: return "Morning"
      case .Afternoon   : return "Afternoon"
      case .Evening  : return "Evening"
      case .Night : return "Night"
    }
  }
  
}
