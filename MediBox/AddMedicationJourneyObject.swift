//
//  AddMedicationJourneyObject.swift
//  MediBox
//
//  Created by Sam Stone on 08/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

public class AddMedicationJourneyObject {
  
  private var _view : UIView
  var view : UIView{
    set {
      self._view = newValue
    }
    get {
      return self._view
    }
  }
  
  private var _titleText : String
  var titleText : String{
    set {
      self._titleText = newValue
    }
    get {
      return self._titleText
    }
  }
  
  private var _buttonText : String
  var buttonText : String{
    set {
      self._buttonText = newValue
    }
    get {
      return self._buttonText
    }
  }
  
  private var _image : String
  var image : String{
    set {
      self._image = newValue
    }
    get {
      return self._image
    }
  }
  
  public init (view: UIView, titleText: String, buttonText: String, image: String) {
    self._view = view
    self._titleText = titleText
    self._buttonText = buttonText
    self._image = image
  }
}