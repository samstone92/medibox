//
//  MBBaseService.swift
//  MediBox
//
//  Created by Sam Stone on 15/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss
import Alamofire

class MBBaseService{
  static let headers = ["Authorization": "Bearer \(FBSDKAccessToken.currentAccessToken().tokenString)"]
  
  static func getRequestArray(url url: String, parameters: [String: AnyObject]? = nil, completion: ([JSON]?) -> Void) {
    self.requestArray(.GET, url: url, parameters: parameters, completion:  completion)
  }
  
  static func getRequest(url url: String, parameters: [String: AnyObject]? = nil, completion: (JSON?) -> Void) {
    self.request(.GET, url: url, parameters: parameters, completion:  completion)
  }
  
  static func postRequest(url url: String, parameters: [String: AnyObject]? = nil, completion: (JSON?) -> Void) {
    self.request(.POST, url: url, parameters: parameters, completion:  completion)
  }
  
  static func putRequest(url url: String, parameters: [String: AnyObject]? = nil, completion: (JSON?) -> Void) {
    self.request(.PUT, url: url, parameters: parameters, completion:  completion)
  }
  
  static private func requestArray(method: Alamofire.Method, url: String, parameters: [String: AnyObject]? = nil, completion: ([JSON]?) -> Void) {
    print(headers)
    Alamofire
      .request(method, url, parameters: parameters,encoding: .JSON ,headers :headers)
      .responseJSON { response in
        debugPrint(response.response)
        debugPrint(response)
        
        if let json = response.result.value as? JSON {
          completion([json])
          return
        }
        
        if let json = response.result.value as? [JSON] {
          completion(json)
          return
        }
        
        
        completion(nil)
    }
  }
  
  static private func request(method: Alamofire.Method, url: String, parameters: [String: AnyObject]? = nil, completion: (JSON?) -> Void) {
    print(headers)

    Alamofire
      .request(method, url, parameters: parameters, encoding: .JSON,headers :headers)
      .responseJSON { response in
        debugPrint(response.response)
        debugPrint(response)
        
        if let json = response.result.value as? JSON {
          completion(json)
          return
        }
        
        if let json = response.result.value as? JSON {
          completion(json)
          return
        }
        
        
        completion(nil)
    }
  }
  
}
