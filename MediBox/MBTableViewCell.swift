//
//  MBTableViewCell.swift
//  MediBox
//
//  Created by Sam Stone on 13/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class MBTableViewCell :UITableViewCell{
  
  @IBOutlet var pointsButton: MBPointsButton!
  @IBOutlet var titleCell: UILabel!
  var cellText : String{
    set {
      self.titleCell.text = newValue
    }
    get {
      return self.titleCell.text!
    }
  }
  
  @IBOutlet var medicationImage: UIImageView!
  var cellImage : UIImage{
    set {
      self.medicationImage.image = newValue
    }
    get {
      return self.medicationImage.image!
    }
  }
  
  @IBOutlet var dosageText: UILabel!
  var cellDossage : String{
    set {
      self.dosageText.text = newValue
    }
    get {
      return self.dosageText.text!
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    loadViewFromNib()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    loadViewFromNib()
  }
  
  func loadViewFromNib() {
    let bundle = NSBundle(forClass: self.dynamicType)
    let nib = UINib(nibName: "MBTableViewCell", bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    view.frame = bounds
    view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
    self.addSubview(view);
  }
  
  override func drawRect(rect: CGRect) {
    self.layer.addBorder(UIRectEdge.Bottom, color: UIColor.lightGrayColor(), thickness: 0.5)

  }
  
 
  
  

  
  
}