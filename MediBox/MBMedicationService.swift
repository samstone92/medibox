//
//  MBMedicationService.swift
//  MediBox
//
//  Created by Sam Stone on 24/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss
import Alamofire
import UIKit

class MBMedicationService :MBBaseService{
  
  static func getMedicationForDate(email: String, date :NSDate, completion: (Bool, [MBMedicationTaken]?) -> Void) {
    let dateString = NSDate().getJSONStringFormat(NSDate().setDateToMidnight(date))
    let url = "https://medbox.herokuapp.com/medicationForDate?email=\(email)&date=\(dateString)"
    getRequestArray(url: url, completion: {(result: [JSON]?) in
      var medications: [MBMedicationTaken] = []
      if result != nil {
        
        for json: JSON in result! {
          if let medication = MBMedicationTaken(json: json) {
            medications.append(medication)
          }
        }
        
        completion(true, medications)
      } else {
        completion(false, nil)
      }
    })
  }
  
  static func getMedicationForUser(email: String, completion: (Bool, [MBMedication]?) -> Void) {
    let url = "https://medbox.herokuapp.com/medicationForUser/\(email)"
    getRequestArray(url: url, completion: {(result: [JSON]?) in
      var medications: [MBMedication] = []
      if result != nil {
        
        for json: JSON in result! {
          if let medication = MBMedication(json: json) {
            medications.append(medication)
          }
        }
        
        completion(true, medications)
      } else {
        completion(false, nil)
      }
    })
  }
  
  static func addMedication(parameters : [String: AnyObject]? = nil, completion: (Bool) -> Void) {
    let url = "https://medbox.herokuapp.com/medication"
    postRequest(url: url, parameters: parameters, completion: {(result: JSON?) in
      completion(true)
    })
  }
  
  static func putMedication(id: String, parameters : [String: AnyObject]? = nil, completion: (Bool) -> Void) {
    let url = "https://medbox.herokuapp.com/medication/\(id)"
    putRequest(url: url, parameters: parameters, completion: {(result: JSON?) in
      completion(true)
    })
  }
  
}
