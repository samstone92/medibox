//
//  UIColor+appColors.swift
//  MediBox
//
//  Created by Sam Stone on 02/07/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
  class func appOrangeColor() -> UIColor {
    return UIColor(red: 250.0/255.0, green: 165.0/255.0, blue: 27.0/255.0, alpha: 1.0)
  }
  
  class func appGreenColor()-> UIColor {
    return UIColor(red: 113.0/255.0, green: 201.0/255.0, blue: 192.0/255.0, alpha: 1.0)
  }
}