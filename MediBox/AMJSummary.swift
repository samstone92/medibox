//
//  AMJSummary.swift
//  MediBox
//
//  Created by Sam Stone on 08/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

public class AMJSummary :UIView{
  
  @IBOutlet var name: UILabel!
  @IBOutlet var dosage: UILabel!
  @IBOutlet var frequency: UILabel!
  
  
  private var _medication : MBMedication!
  var medication : MBMedication{
    set {
      self._medication = newValue
      setText()
    }
    get {
      return self._medication!
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setText(){
    name.text = self._medication.name == nil ? "" : self._medication.name
    
    if _medication.dosage != nil && _medication.measurement != nil{
      dosage.text = "\(self._medication.dosage)) \(DosageTypes(rawValue: self._medication.measurement)!.description)"
    }
    frequency.text = self._medication.time == nil ? "" : DayPartType(rawValue: self._medication.time!)?.description
    
  }
  
  func setup(){
    loadViewFromNib()
  }
  
  func loadViewFromNib() {
    let bundle = NSBundle(forClass: self.dynamicType)
    let nib = UINib(nibName: "AMJSummary", bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    view.frame = bounds
    view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
    self.addSubview(view);
  }
}
