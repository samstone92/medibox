//
//  MBMedication.swift
//  MediBox
//
//  Created by Sam Stone on 24/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

public struct MBMedication: Glossy {
  
  public var id : String!
  public var createDate : String!
  public var dosage : Int!
  public var measurement : Int!
  public var name : String!
  public var time : Int!
  public var user : String!
  
  public init(){
    
  }
  
  public init?(json: JSON){
    id = "_id" <~~ json
    createDate = "createDate" <~~ json
    dosage = "dosage" <~~ json
    measurement = "measurement" <~~ json
    name = "name" <~~ json
    time = "time" <~~ json
    user = "user" <~~ json
  }
  
  
  //MARK: Encodable
  public func toJSON() -> JSON? {
    return jsonify([
      "_id" ~~> id,
      "createDate" ~~> createDate,
      "dosage" ~~> dosage,
      "measurement" ~~> measurement,
      "name" ~~> name,
      "time" ~~> time,
      "user" ~~> user,
      ])
  }
  
}
