//
//  MBFacebookServices.swift
//  MediBox
//
//  Created by Sam Stone on 15/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation

class MBFacebookServices{
  
  private static var tokenString : String?

  static func getUserDetails(tokenString :String, completion: (Bool, FBUser!) -> Void){
    let req = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"email,name"], tokenString: tokenString, version: nil, HTTPMethod: "GET")
    req.startWithCompletionHandler({ (connection, result, error : NSError!) -> Void in
      if(error == nil)
      {
        let userDictionary = result as! NSDictionary
        let fbUser  = FBUser.init(dictionary: userDictionary)
        
        completion(true, fbUser)
      }
      else
      {
        print("error \(error)")
      }
    })
  }

  static func saveFacebookResults(result :FBSDKLoginManagerLoginResult){
    self.tokenString = result.token.tokenString
  }
  
  static func getTokenString()->String{
    return tokenString!
  }

}

