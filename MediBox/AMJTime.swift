//
//  AMJTime.swift
//  MediBox
//
//  Created by Sam Stone on 08/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class AMJTime :UIView, UIPickerViewDataSource, UIPickerViewDelegate{
  
  var delegate:MBSetTime! = nil
  
  @IBOutlet var dayPartPicker: UIPickerView!
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setup(){
    loadViewFromNib()
    self.dayPartPicker.delegate = self
    self.dayPartPicker.dataSource = self
    
  }
  
  func loadViewFromNib() {
    let bundle = NSBundle(forClass: self.dynamicType)
    let nib = UINib(nibName: "AMJTime", bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    view.frame = bounds
    view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
    self.addSubview(view);
  }
  
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return DayPartType.count.hashValue
  }
  
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return DayPartType(rawValue: row)?.description;
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
    delegate.didSetTime(DayPartType(rawValue: row)!)
  }

}
