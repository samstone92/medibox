//
//  MBUser.swift
//  MediBox
//
//  Created by Sam Stone on 15/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

public struct MBUser: Glossy {
  
  public var id : String?
  public var createDate : String?
  public var email : String!
  public var fullName : String?
  public var UUID : String?
  
  public init?(json: JSON){
    id = "_id" <~~ json
    createDate = "createDate" <~~ json
    email = "email" <~~ json
    fullName = "fullName" <~~ json
    UUID = "UUID" <~~ json
  }
  
  public init?(fbUser :FBUser){
    self.email = fbUser.email
    self.fullName = fbUser.name
  }
  
  //MARK: Encodable
  public func toJSON() -> JSON? {
    return jsonify([
      "_id" ~~> id,
      "createDate" ~~> createDate,
      "email" ~~> email,
      "fullName" ~~> fullName,
      "UUID" ~~> UUID
      ])
  }
  
}