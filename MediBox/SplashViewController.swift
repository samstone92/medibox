//
//  SplashViewController.swift
//  MediBox
//
//  Created by Sam Stone on 03/07/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import UIKit

class SplashViewController : UIViewController,UIScrollViewDelegate {
  
  var splashScreenObjects = [SplashScreenObject]()
  
  var imageViewArray = [UIImageView]()
  var subtitleViewArray = [UILabel]()
  
  @IBOutlet var navTitle: UILabel!
  @IBOutlet var add: UIButton!
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var pageControl: UIPageControl!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    createSplashScreenObjects()
    configurePageControl()
  }
  
  func createSplashScreenObjects() {
    let firstScreen: SplashScreenObject = SplashScreenObject(subtitle: "Medication reminders on your phone. Never miss your next dose", image: UIImage(named: "splashScreen1")!)
    let secondScreen: SplashScreenObject = SplashScreenObject(subtitle: "Track how good you have been with your medication", image: UIImage(named: "splashScreen2")!)
    let thirdScreen: SplashScreenObject = SplashScreenObject(subtitle: "The better you are with your medication, the more points you'll earn!", image: UIImage(named: "splashScreen3")!)
    splashScreenObjects.append(firstScreen)
    splashScreenObjects.append(secondScreen)
    splashScreenObjects.append(thirdScreen)

  }
  
  override func viewDidLayoutSubviews() {
    configureScrollView()
  }
  
  func configureScrollView() {
     self.scrollView.frame = CGRectMake(0, self.navTitle.frame.size.height + 8, self.view.frame.size.width, self.view.frame.size.height - self.add.frame.size.height - self.pageControl.frame.size.width - 32)

    self.scrollView.showsHorizontalScrollIndicator = false
    self.scrollView.showsVerticalScrollIndicator = false

    self.scrollView.pagingEnabled = true
    self.scrollView.delegate = self
    
    let width = view.frame.size.width
    
    for index in 0..<splashScreenObjects.count {
      let subtitle = UILabel(frame: CGRectMake((width * CGFloat(index)) + 25, self.scrollView.frame.size.height-75, width-50, 75))
      subtitle.text = splashScreenObjects[index].subtitle
      subtitle.textAlignment = NSTextAlignment.Center
      subtitle.textColor = UIColor.whiteColor()
      subtitle.font = UIFont(name:"Ubuntu", size: 16)
      subtitle.numberOfLines = 2
      subtitle.backgroundColor = UIColor.clearColor()
      self.scrollView.addSubview(subtitle)
      self.subtitleViewArray.append(subtitle)
      subtitle.alpha = 0
      
      let mainImage = UIImageView(frame: CGRectMake((width * CGFloat(index)), 50, width, self.scrollView.frame.size.height-150))
      mainImage.image = splashScreenObjects[index].image
      mainImage.contentMode = UIViewContentMode.ScaleAspectFit
      self.scrollView.addSubview(mainImage)
      self.imageViewArray.append(mainImage)
      mainImage.alpha = 0
    }
    
    self.scrollView.contentSize = CGSizeMake(width * CGFloat(splashScreenObjects.count), self.scrollView.frame.size.height-50)
    animateViews(Int(0))

  }
  
  func configurePageControl() {
    self.pageControl.numberOfPages = splashScreenObjects.count
    self.pageControl.currentPage = 0
    self.view.addSubview(pageControl)
    pageControl.addTarget(self, action: #selector(SplashViewController.changePage(_:)), forControlEvents: UIControlEvents.ValueChanged)
  }
  
  func changePage(sender: AnyObject) -> () {
    let x = CGFloat(pageControl.currentPage) * self.view.frame.size.width
    scrollView.setContentOffset(CGPointMake(x, 0), animated: true)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    let pageNumber = round(scrollView.contentOffset.x / self.view.frame.size.width)
    pageControl.currentPage = Int(pageNumber)
    animateViews(Int(pageNumber))
  }
  
  func animateViews(pageNumber: Int) {
    UIView.animateWithDuration(0.5, animations: {
      self.imageViewArray[pageNumber].alpha = 1.0
      self.subtitleViewArray[pageNumber].alpha = 1.0
    })
  }
  
  @IBAction func closeButton(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  
  @IBAction func addMedicationPressed(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
}
