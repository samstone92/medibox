//
//  MBAddMedicationRootViewController.swift
//  MediBox
//
//  Created by Sam Stone on 08/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import Gloss

class MBAddMedicationRootViewController : UIViewController, UIScrollViewDelegate, MBTimeLineProtocol, MBSetName, MBSetDose, MBSetTime{
  
  var viewArray = [AddMedicationJourneyObject]()
  @IBOutlet var scrollView: UIScrollView!
  @IBOutlet var nextButton: UIButton!
  @IBOutlet var timeLine: MBTimeLine!
  
  let summaryView = AMJSummary.init()

  var currentView : Int = 0
  var lastContentOffset : CGFloat = 0
  var hasRun = false
  
  var medication  = MBMedication.init()


  override func viewDidLoad() {
    UINavigationBar.appearance().shadowImage = UIImage()
    UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: .Default)
    setupViewArray()
    self.hideKeyboardWhenTappedAround()
  }
  
  
  func setupViewArray(){
    let nameView = AMJName.init()
    nameView.delegate = self
    
    let doseView = AMJDose.init()
    doseView.delegate = self
    
    let timeView = AMJTime.init()
    timeView.delegate = self
        
    let name = AddMedicationJourneyObject.init(view: nameView, titleText: "Add Medication", buttonText: "Next", image: "tag-white")
    let dose = AddMedicationJourneyObject.init(view: doseView, titleText: "Add Medication", buttonText: "Next", image: "jug-white")
    let time = AddMedicationJourneyObject.init(view: timeView, titleText: "Add Medication", buttonText: "Next", image: "calendar-white")
    let summary = AddMedicationJourneyObject.init(view: summaryView, titleText: "Add Medication", buttonText: "Done", image: "checked-white")
    
    viewArray.append(name)
    viewArray.append(dose)
    viewArray.append(time)
    viewArray.append(summary)
  }
  
  func setupTimeLine(){
    self.timeLine.segmentArray = viewArray
    self.timeLine.delegate = self
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    setupScrollView()
    if hasRun == false{
      setupTimeLine()
    }
    self.hasRun = true
  }
  
  
  func setupScrollView() {
    self.scrollView.scrollEnabled = false
    self.scrollView.frame = CGRectMake(0, self.timeLine.frame.size.height, self.view.frame.size.width, self.view.frame.size.height - self.nextButton.frame.size.height - self.timeLine.frame.size.height)
    
    self.scrollView.showsHorizontalScrollIndicator = false
    self.scrollView.showsVerticalScrollIndicator = false
    
    self.scrollView.pagingEnabled = true
    self.scrollView.delegate = self
    
    let width = self.scrollView.frame.size.width
    for index in 0..<viewArray.count {
      let view = viewArray[index].view
      view.frame = CGRectMake((width * CGFloat(index)), 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height)
      self.scrollView.addSubview(view)
    }
    self.scrollView.contentSize = CGSizeMake(width * CGFloat(viewArray.count), self.scrollView.frame.size.height)
  }

  @IBAction func nextButtonPressed(sender: AnyObject) {
    changePage(sender)
  }
  
  func changePage(sender: AnyObject){
    if self.currentView < self.viewArray.count-1{
      self.currentView += 1
      let x = CGFloat(self.currentView) * self.view.frame.size.width
      scrollView.setContentOffset(CGPointMake(x, 0), animated: true)
      self.timeLine.moveToPositionAtId(self.currentView)
    } else {
      saveMedication()
    }
  }
  
  func saveMedication(){
    medication.user = NSUserDefaults().getCurrentUserEmail()
    MBMedicationService.addMedication(medication.toJSON()) { (completion :Bool) in
      self.dismissViewControllerAnimated(true, completion: {});
    }
  }
  
  @IBAction func close(sender: AnyObject) {
    self.dismissViewControllerAnimated(true, completion: {});
  }
  
  //MARK: - Delegates
  
  func didSelectSegmentWithId(id: Int) {
    self.currentView = id
    let x = CGFloat(self.currentView) * self.view.frame.size.width
    scrollView.setContentOffset(CGPointMake(x, 0), animated: true)
    self.timeLine.moveToPositionAtId(self.currentView)
  }
  
  func didSetName(name: String) {
    self.medication.name = name
    self.summaryView.medication = self.medication
  }
  
  func didSetTime(time: DayPartType) {
    self.medication.time = time.rawValue
    self.summaryView.medication = self.medication
  }
  
  func didSetDose(amount :Int, dosageType:DosageTypes) {
    self.medication.dosage = amount
    self.medication.measurement = dosageType.rawValue
    self.summaryView.medication = self.medication
  }
  
}
