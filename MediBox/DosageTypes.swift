//
//  File.swift
//  MediBox
//
//  Created by Sam Stone on 11/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

enum DosageTypes: Int, CustomStringConvertible {
  case Milligrams = 0
  case Tablets = 1
  case Mililitres = 2
  case Spoonfuls = 3
  case Sachets = 4
  static var count: Int { return DosageTypes.Sachets.hashValue + 1 }
  
  var description: String {
    switch self {
      case .Milligrams: return "Milligrams"
      case .Tablets   : return "Tablets"
      case .Mililitres  : return "Mililitres"
      case .Spoonfuls : return "Spoonfuls"
      case .Sachets : return "Sachets"
    }
  }
  
  var image: UIImage{
    switch self{
      case .Milligrams: return UIImage(named: "mgs")!
      case .Tablets   : return UIImage(named: "pill")!
      case .Mililitres  : return UIImage(named: "mil")!
      case .Spoonfuls : return UIImage(named: "spoonful")!
      case .Sachets : return UIImage(named: "jug")!
    }
  }
}
