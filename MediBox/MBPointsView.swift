//
//  MBPointsView.swift
//  MediBox
//
//  Created by Sam Stone on 13/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class MBPointsView :UIView{
  
  private var _points : Int!
  var points : Int{
    set {
      self._points = newValue
      pointsText?.text = String(self._points)
    }
    get {
      return self._points!
    }
  }
  
  var pointsText : UILabel?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setup(){
    
    
    let patternImage = UIImageView.init(frame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
    self.addSubview(patternImage)
  

    self.pointsText = UILabel.init(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
    self.pointsText!.textAlignment = NSTextAlignment.Center
    self.addSubview(self.pointsText!)


  }
  
}
