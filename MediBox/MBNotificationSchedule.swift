//
//  MBNotificationSchedule.swift
//  MediBox
//
//  Created by Sam Stone on 03/09/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

public struct MBNotificationSchedule: Glossy {
  
  public var id : String!
  public var reminders : [String]!
  public var user : String!

  public init?(json: JSON){
    id = "_id" <~~ json
    reminders = "reminders" <~~ json
    user = "user" <~~ json
  }
  
  public func toJSON() -> JSON? {
    return jsonify([
      "_id" ~~> id,
      "reminders" ~~> reminders,
      "user" ~~> user,
      ])
  }
  
}
