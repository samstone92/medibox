//
//  MBTextField.swift
//  MediBox
//
//  Created by Sam Stone on 09/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class MBTextField :UITextField{
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)!
  }
  
  
  override func drawRect(rect: CGRect) {
    setup()
  }
  
  func setup(){
    let border = CALayer()
    let width = CGFloat(2.0)
    border.borderColor = UIColor.appGreenColor().CGColor
    border.frame = CGRect(x: 0, y: self.frame.size.height-width, width: self.frame.size.width, height: width)
    border.borderWidth = width
    self.layer.addSublayer(border)
    self.layer.masksToBounds = false
    self.borderStyle = UITextBorderStyle.None;
  }
  
}