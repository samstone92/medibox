//
//  MBTimeLine.swift
//  MediBox
//
//  Created by Sam Stone on 09/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

public class MBTimeLine :UIView{
  
  private var _segmentArray = [AddMedicationJourneyObject]()
  var segmentArray : [AddMedicationJourneyObject]{
    set {
      self._segmentArray = newValue
      setup()
    }
    get {
      return self._segmentArray
    }
  }
  
  private var idVisted = [Int]()
  private var segmentViewArray  = [MBTimeLineSegment]()
  private var underLine : UIView?
  private var segmentWidth :CGFloat = 0
  var delegate:MBTimeLineProtocol! = nil

  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  
  private func setup(){
    setupTimeline()
    setupUnderline()
  }
  
  func setupTimeline(){
    self.segmentWidth = CGFloat(self.frame.size.width/CGFloat(self._segmentArray.count))
    var x = CGFloat(0)
    for index in 0..<self._segmentArray.count {
      let view = MBTimeLineSegment.init(frame: CGRectMake(x, 0, segmentWidth, self.frame.size.height))
      view.image = self._segmentArray[index].image
      self.addSubview(view)
      view.tag = index
      segmentViewArray.append(view)
      let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MBTimeLine.segmentTapped(_:)))
      view.addGestureRecognizer(tapGesture)
      x = x + segmentWidth
    }
    addIdToVisted(0)
  }
  
  func segmentTapped(sender: AnyObject){
    if self.idVisted.contains(sender.view.tag){
      delegate!.didSelectSegmentWithId(sender.view.tag)
    }
  }
  
  func setupUnderline(){
    self.underLine = UIView.init(frame: CGRectMake(0, self.frame.size.height+5, self.segmentWidth, 2))
    self.underLine!.backgroundColor = UIColor.appOrangeColor()
    self.addSubview(self.underLine!)
  }
  
  public func moveToPositionAtId(id : Int){
    UIView.animateWithDuration(0.4, animations: {
      self.underLine!.frame.origin.x = (self.frame.size.width/CGFloat(self._segmentArray.count)) * CGFloat(id)
    })
    addIdToVisted(id)
  }
  
  func addIdToVisted(id :Int){
    if !self.idVisted.contains(id){
      self.idVisted.append(id)
      self.segmentViewArray[id].setSegmentSelected()
    }
  }
}

protocol MBTimeLineProtocol :class{
  func didSelectSegmentWithId(id:Int)
}