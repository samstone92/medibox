//
//  MBTimePickerTextView.swift
//  MediBox
//
//  Created by Sam Stone on 02/09/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

public class MBTimePickerTextView : UITextField, UITextFieldDelegate{
  var timePickerDelegate:MBTimePickerTextViewProtocol! = nil
  let datePickerView:UIDatePicker = UIDatePicker()
  
  override init(frame: CGRect) {
    super.init(frame: frame);
    delegate = self
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder);
    delegate = self
  }
  
  public func textFieldDidBeginEditing(textField: UITextField) {
    datePickerView.datePickerMode = UIDatePickerMode.Time
    self.inputView = datePickerView
    datePickerView.addTarget(self, action: #selector(MBTimePickerTextView.valueChanged(_:)), forControlEvents: UIControlEvents.ValueChanged)
  }
  
  func valueChanged(sender:UIDatePicker){
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateStyle = NSDateFormatterStyle.NoStyle
    dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
    self.text = dateFormatter.stringFromDate(sender.date)
    timePickerDelegate!.pickerValueChanged(self.tag, date: sender.date)
  }
  
  public override func caretRectForPosition(position: UITextPosition) -> CGRect {
    return CGRectZero;
  }
  
  public func setDate(date :NSDate){
    datePickerView.date = date
    valueChanged(datePickerView)
  }
  
}

protocol MBTimePickerTextViewProtocol :class{
  func pickerValueChanged(tag:Int, date: NSDate)
}
