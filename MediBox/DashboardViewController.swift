//
//  DashboardViewController.swift
//  MediBox
//
//  Created by Sam Stone on 08/07/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class DashboardViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
  
  @IBOutlet var tableView: UITableView!
  
  var shouldShowSplash = true;
  var medicationArray = [MBMedicationTaken]()
  var morningArray = [MBMedicationTaken]()
  var afternoonArray = [MBMedicationTaken]()
  var eveningArray = [MBMedicationTaken]()
  var nightArray = [MBMedicationTaken]()
  
  let cellReuseIdendifier = "cell"

  override func viewDidLoad() {
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor(), NSFontAttributeName: UIFont(name: "Ubuntu", size: 25)!]
    showSplashScreen()
    setupTableView()
    setupPoints()
  }
  
  func setupTableView() {
    self.tableView.sectionHeaderHeight = UITableViewAutomaticDimension
    self.tableView.estimatedSectionHeaderHeight = 60
    self.tableView.rowHeight = 60
    self.tableView.delegate = self
    self.tableView.dataSource = self
    self.tableView.backgroundColor = UIColor(patternImage: UIImage(named: "background")!)
    
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None;
    
    tableView.registerClass(MBTableViewCell.self, forCellReuseIdentifier: cellReuseIdendifier)
  }
  
  func getMedication() {
    emptyArrays()
    MBMedicationService.getMedicationForDate(NSUserDefaults().getCurrentUserEmail(), date: NSDate()) { (complete :Bool, medication :[MBMedicationTaken]?) in
      if medication != nil {
        for med in medication!{
          self.medicationArray.append(med)
        }
        self.oragniseData()
        self.tableView.reloadData()
      }
    }
  }
  
  func setupPoints(){
    let customPointsView = MBPointsView.init(frame:CGRectMake(0,-100, self.view.frame.size.width, 100))
    customPointsView.points = 20
    self.tableView.addSubview(customPointsView)
  }
  
  override func viewDidAppear(animated: Bool) {
    getMedication()
    self.tableView.reloadData()
  }
  
  func showSplashScreen() {
    if shouldShowSplash{
      let vc = self.storyboard?.instantiateViewControllerWithIdentifier("splashScreen") as! SplashViewController
      self.presentViewController(vc, animated: true, completion: nil)
      shouldShowSplash = false
    }
  }
  
  func emptyArrays(){
    self.medicationArray.removeAll()
    self.morningArray.removeAll()
    self.afternoonArray.removeAll()
    self.eveningArray.removeAll()
    self.nightArray.removeAll()
  }

  func scrollViewDidScroll(scrollView: UIScrollView) {
    let offset = self.tableView.contentOffset;
    let bounds = self.tableView.bounds;
    let size = self.tableView.contentSize.height;
    let inset = self.tableView.contentInset;
    let y = offset.y + bounds.size.height - inset.bottom;
    let h = size;
    
    if (y > h) {
      if size < self.tableView.frame.size.height{
        self.tableView.alpha = 1 - (offset.y/100);

      } else {
        self.tableView.alpha = 1 - ((y-size)/100);
      }
    } else {
    
      self.tableView.alpha = 1;
    }
  }


  func oragniseData(){
    for medication in medicationArray {
      switch medication.time{
      case DayPartType.Morning.rawValue:
        morningArray.append(medication)
        break
      case DayPartType.Afternoon.rawValue:
        afternoonArray.append(medication)
        break
      case DayPartType.Evening.rawValue:
        eveningArray.append(medication)
        break
      case DayPartType.Night.rawValue:
        nightArray.append(medication)
        break
      default:
        break
      }
    }
    
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    var noRows = 0
    switch section {
    case DayPartType.Morning.rawValue:
      noRows = morningArray.count
      break
    case DayPartType.Afternoon.rawValue:
      noRows = afternoonArray.count
      break
    case DayPartType.Evening.rawValue:
      noRows = eveningArray.count
      break
    case DayPartType.Night.rawValue:
      noRows = nightArray.count
      break
    default:
      break
    }
    return noRows
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return DayPartType.count
  }
  
  func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String?
  {
    var sectionName = ""
    switch section {
    case DayPartType.Morning.rawValue:
      sectionName = DayPartType.Morning.description
      break
    case DayPartType.Afternoon.rawValue:
      sectionName = DayPartType.Afternoon.description
      break
    case DayPartType.Evening.rawValue:
      sectionName = DayPartType.Evening.description
      break
    case DayPartType.Night.rawValue:
      sectionName = DayPartType.Night.description
      break
    default:
      break
    }
    return sectionName
  }


  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = MBTableSectionHeader.init(frame: CGRectMake(0, 0, self.tableView.frame.size.width, self.tableView.frame.size.width))
    header.headerText = DayPartType(rawValue: section)!.description
    header.layer.zPosition = 0
    return header
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(cellReuseIdendifier, forIndexPath: indexPath) as! MBTableViewCell
    switch indexPath.section {
    case DayPartType.Morning.rawValue:
      cell.cellText = morningArray[indexPath.row].name
      cell.cellImage = (DosageTypes(rawValue: morningArray[indexPath.row].measurement)?.image)!
      cell.cellDossage = "\(morningArray[indexPath.row].dosage) \(DosageTypes(rawValue: morningArray[indexPath.row].measurement)!.description)"
      cell.pointsButton.medication = morningArray[indexPath.row]
      break
    case DayPartType.Afternoon.rawValue:
      cell.cellText = afternoonArray[indexPath.row].name
      cell.cellImage = (DosageTypes(rawValue: afternoonArray[indexPath.row].measurement)?.image)!
      cell.cellDossage = "\(afternoonArray[indexPath.row].dosage) \(DosageTypes(rawValue: afternoonArray[indexPath.row].measurement)!.description)"
      cell.pointsButton.medication = afternoonArray[indexPath.row]
      break
    case DayPartType.Evening.rawValue:
      cell.cellText = eveningArray[indexPath.row].name
      cell.cellImage = (DosageTypes(rawValue: eveningArray[indexPath.row].measurement)?.image)!
      cell.cellDossage = "\(eveningArray[indexPath.row].dosage) \(DosageTypes(rawValue: eveningArray[indexPath.row].measurement)!.description)"
      cell.pointsButton.medication = eveningArray[indexPath.row]
      break
    case DayPartType.Night.rawValue:
      cell.cellText = nightArray[indexPath.row].name
      cell.cellImage = (DosageTypes(rawValue: nightArray[indexPath.row].measurement)?.image)!
      cell.cellDossage = "\(nightArray[indexPath.row].dosage) \(DosageTypes(rawValue: nightArray[indexPath.row].measurement)!.description)"
      cell.pointsButton.medication = nightArray[indexPath.row]
      break
    default: break
    }
   
    return cell
  }
  
  func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    return true
  }
  
  func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    
  }
  
  func updateMedication(){
    
  }

  
}
