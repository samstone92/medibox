//
//  File.swift
//  MediBox
//
//  Created by Sam Stone on 15/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss
import Alamofire
import UIKit

class MBUserService :MBBaseService{
  
  static func updateUser(email: String, parameters : [String: AnyObject]? = nil, completion: (Bool) -> Void) {
    let url = "https://medbox.herokuapp.com/users/\(email)"
    putRequest(url: url, parameters: parameters, completion: {(result: JSON?) in
      completion(true)
    })
    
  }
  
  static func getUserByEmail(email: String, completion: (Bool, MBUser?) -> Void) {
    let url = "https://medbox.herokuapp.com/users/\(email)"
    getRequest(url: url, completion: {(result: JSON?) in
      var user: MBUser?
      if result != nil {
        user = MBUser(json: result!)
        completion(true, user)
      } else {
        completion(false, nil)
      }
    })
    
  }
  
  static func addUser(parameters : [String: AnyObject]? = nil, completion: (Bool) -> Void) {
    let url = "https://medbox.herokuapp.com/users"
    postRequest(url: url, parameters: parameters, completion: {(result: JSON?) in
        completion(true)
    })
    
  }
  
}
