//
//  SplashScreenObject.swift
//  MediBox
//
//  Created by Sam Stone on 03/07/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import UIKit

class SplashScreenObject {
  
  var subtitle: String?
  var image: UIImage?
  
  init(subtitle: String, image: UIImage) {
    self.subtitle = subtitle
    self.image = image
  }
  
  func setSubtitle(subtitle: String) {
    self.subtitle = subtitle
  }
 
  func setImage(image: UIImage) {
    self.image = image
  }
  
}