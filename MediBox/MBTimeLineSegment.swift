//
//  MBTimeLineSegment.swift
//  MediBox
//
//  Created by Sam Stone on 09/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

public class MBTimeLineSegment: UIView {
  
  private var backgroundView : UIView?
  private var filledImage : UIImageView!
  private var imageView : UIImageView!
  
  private var _image : String!
  var image : String{
    set {
      self._image = newValue
      setupImageView()
    }
    get {
      return self._image
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setup(){
    self.clipsToBounds = true
    self.backgroundColor = UIColor.lightGrayColor()
    self.layer.borderColor = UIColor.whiteColor().CGColor
    self.layer.borderWidth = 0.5
  }
  
  func setupImageView(){
    self.backgroundView = UIView.init(frame: CGRectMake(-self.frame.size.width, 0, self.frame.size.width, self.frame.size.height))
    self.backgroundView!.backgroundColor = UIColor.appOrangeColor()
    self.addSubview(self.backgroundView!)
    
    self.imageView = UIImageView.init(frame: CGRectMake(0, 4, self.frame.size.width, self.frame.size.height-8))
    self.imageView.image = UIImage.init(named: self._image)
    self.imageView.contentMode = UIViewContentMode.ScaleAspectFit
    self.addSubview(self.imageView)
    
    self.filledImage = UIImageView.init(frame: CGRectMake(0, 4, self.frame.size.width, self.frame.size.height-8))
    self.filledImage.image = UIImage.init(named: "\(self._image)-filled")
    self.filledImage.contentMode = UIViewContentMode.ScaleAspectFit
    self.filledImage.alpha = 0
    self.addSubview(self.filledImage)
  }
  
  public func setSegmentSelected(){
    UIView.animateWithDuration(0.4, animations: {
      self.backgroundView!.frame.origin.x += self.frame.size.width
      self.filledImage.alpha = 1
      self.imageView.alpha = 0
    })
  }
  
}