//
//  AMJDose.swift
//  MediBox
//
//  Created by Sam Stone on 08/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class AMJDose :UIView, UIPickerViewDataSource, UIPickerViewDelegate{
  
  var delegate:MBSetDose! = nil
  
  @IBOutlet var amount: MBTextField!
  @IBOutlet var dosageTypePicker: UIPickerView!
  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setup(){
    loadViewFromNib()
    self.dosageTypePicker.delegate = self
    self.dosageTypePicker.dataSource = self
  }
  
  func loadViewFromNib() {
    let bundle = NSBundle(forClass: self.dynamicType)
    let nib = UINib(nibName: "AMJDose", bundle: bundle)
    let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
    view.frame = bounds
    view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
    self.addSubview(view);
  }
  
  //MARK: - delegats
  
  func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return DosageTypes.count.hashValue
  }
  
  func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return DosageTypes(rawValue: row)?.description;
  }
  
  func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
    let amountInt:Int? = self.amount.text == "" ? 0 : Int(self.amount.text!)
    delegate.didSetDose(amountInt!, dosageType: DosageTypes(rawValue: row)!)
  }
  
  @IBAction func didEditAmount(sender: AnyObject) {
    delegate.didSetDose(Int(self.amount.text!)!, dosageType: DosageTypes(rawValue: self.dosageTypePicker.selectedRowInComponent(0))!)
  }
 
  
}
