//
//  NSUserDefaults+functions.swift
//  MediBox
//
//  Created by Sam Stone on 15/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation

extension NSUserDefaults{
  
  func saveCurrentUser(user :MBUser){
    NSUserDefaults.standardUserDefaults().setValue(user.fullName, forKey:K.UserDefaultKeys.fullName)
    NSUserDefaults.standardUserDefaults().setValue(user.email, forKey:K.UserDefaultKeys.email)
  }
  
  func getCurrentUserName() -> String{
    return NSUserDefaults.standardUserDefaults().valueForKey(K.UserDefaultKeys.fullName) as! String
  }
  
  func getCurrentUserEmail() -> String{
    return NSUserDefaults.standardUserDefaults().valueForKey(K.UserDefaultKeys.email) as! String
  }
  
  func getUUID() -> String{
    if (NSUserDefaults.standardUserDefaults().valueForKey(K.UserDefaultKeys.UUID) != nil){
      return NSUserDefaults.standardUserDefaults().valueForKey(K.UserDefaultKeys.UUID) as! String
    }
    return ""
  }
  
}
