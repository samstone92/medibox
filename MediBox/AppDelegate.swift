//
//  AppDelegate.swift
//  MediBox
//
//  Created by Sam Stone on 02/07/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
    
    application.registerUserNotificationSettings(settings)
    application.registerForRemoteNotifications()
    

    return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
  }
  
  func application(application: UIApplication,
                   openURL url: NSURL,
                           sourceApplication: String?,
                           annotation: AnyObject) -> Bool {
    return FBSDKApplicationDelegate.sharedInstance().application(
      application,
      openURL: url,
      sourceApplication: sourceApplication,
      annotation: annotation)
  }
  
  func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    let characterSet: NSCharacterSet = NSCharacterSet(charactersInString: "<>")
    
    let deviceTokenString: String = (deviceToken.description as NSString).stringByTrimmingCharactersInSet(characterSet).stringByReplacingOccurrencesOfString( " ", withString: "") as String
    NSUserDefaults.standardUserDefaults().setValue(deviceTokenString, forKey:K.UserDefaultKeys.UUID)
    print(deviceTokenString)
  }

}

