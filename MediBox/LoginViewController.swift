//
//  LoginViewController.swift
//  MediBox
//
//  Created by Sam Stone on 14/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//



import UIKit

class LoginViewController : UIViewController, FBSDKLoginButtonDelegate {
  

  @IBOutlet var facebookLogin: FBSDKLoginButton!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    if (FBSDKAccessToken.currentAccessToken() != nil){
      MBFacebookServices.getUserDetails(FBSDKAccessToken.currentAccessToken().tokenString) { (complete :Bool, fbUser :FBUser!) in
        self.checkIfUserHasAlreadyLoggedIn(fbUser)
      }
    }
    setupFacebookLogin()
  }
  
  
  func setupFacebookLogin(){
    self.facebookLogin.readPermissions = ["public_profile", "email"]
    self.facebookLogin.delegate = self
  }

  func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
    if ((error) != nil){
      return
    }
    
    if result.grantedPermissions.contains("email"){
      MBFacebookServices.getUserDetails(result.token.tokenString) { (complete :Bool, fbUser :FBUser!) in
        self.checkIfUserHasAlreadyLoggedIn(fbUser)
      }
    }
  }
    
  func checkIfUserHasAlreadyLoggedIn(fbUser: FBUser){
    MBUserService.getUserByEmail(fbUser.email, completion: { (succeed :Bool, user: MBUser?) in
      if user == nil{
        var newUser = MBUser.init(fbUser: fbUser)
        newUser?.UUID = NSUserDefaults().getUUID()
        MBUserService.addUser(newUser?.toJSON(), completion: { (complete: Bool) in
          NSUserDefaults().saveCurrentUser(newUser!)
          self.performSegueWithIdentifier("login", sender: nil)
        })
      } else {
        var existingUser = MBUser.init(json: (user?.toJSON())!)
        existingUser?.UUID = NSUserDefaults().getUUID()
        MBUserService.updateUser((existingUser?.email)!, parameters: existingUser?.toJSON(), completion: { (complete:Bool) in
          NSUserDefaults().saveCurrentUser(existingUser!)
          self.performSegueWithIdentifier("login", sender: nil)
        })
      }
    })
  }

  func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
    print("User Logged Out")
  }
  
}

