//
//  UIFont+appFont.swift
//  MediBox
//
//  Created by Sam Stone on 13/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
  
  class func customFontRegular(size :CGFloat) -> UIFont {
    return UIFont(name: "Ubuntu", size: size)!
  }

  class func customFontMedium(size :CGFloat) -> UIFont {
    return UIFont(name: "Ubuntu-medium", size: size)!
  }
  
  class func customFontBold(size :CGFloat) -> UIFont {
    return UIFont(name: "Ubuntu-bold", size: size)!
  }
  
}