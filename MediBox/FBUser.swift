//
//  FBUser.swift
//  MediBox
//
//  Created by Sam Stone on 22/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

public struct FBUser {
  public var id: String!
  public var name: String!
  public var email: String!

  public init?(dictionary: NSDictionary!) {
    self.id = String(dictionary.valueForKey("id")!)
    self.name = String(dictionary.valueForKey("name")!)
    self.email = String(dictionary.valueForKey("email")!)
  }
  
  func toJSON() -> JSON? {
    return jsonify([
      "id" ~~> self.id,
      "name" ~~> self.name,
      "email" ~~> self.email
      ])
  }
}