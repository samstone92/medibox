//
//  NSDate+functions.swift
//  MediBox
//
//  Created by Sam Stone on 14/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

extension NSDate {
  
  func setDateToMidnight(date: NSDate) -> NSDate! {
    let cal: NSCalendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
    return cal.dateBySettingHour(0, minute: 0, second: 0, ofDate: date, options: NSCalendarOptions())!
  }
  
  func getJSONStringFormat(date :NSDate) -> String!{
    let formatter = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    return formatter.stringFromDate(date)
  }
  
  func getDateFromJSONString(date :String) -> NSDate!{
    let dateFormatter = NSDateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    return dateFormatter.dateFromString(date)
  }

}
