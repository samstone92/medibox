//
//  MBNotificationScheduleService.swift
//  MediBox
//
//  Created by Sam Stone on 03/09/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss
import Alamofire
import UIKit

class MBNotificationScheduleService :MBBaseService{
  
  static func getNotificationForUser(email: String, completion: (Bool, MBNotificationSchedule?) -> Void) {
    let url = "https://medbox.herokuapp.com/remindersForUser/\(email)"
    getRequest(url: url, completion: {(result: JSON?) in
      var user: MBNotificationSchedule?
      if result != nil {
        user = MBNotificationSchedule(json: result!)
        completion(true, user)
      } else {
        completion(false, nil)
      }
    })
  }
  
  static func updateSchedule(email: String, parameters : [String: AnyObject]? = nil, completion: (Bool) -> Void) {
    let url = "https://medbox.herokuapp.com/reminders/\(email)"
    putRequest(url: url, parameters: parameters, completion: {(result: JSON?) in
      completion(true)
    })
    
  }
  
}