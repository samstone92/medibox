//
//  AMJProtocols.swift
//  MediBox
//
//  Created by Sam Stone on 10/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

protocol MBSetName :class{
  func didSetName(name:String)
}

protocol MBSetDose:class{
  func didSetDose(amount :Int, dosageType:DosageTypes)
}

protocol MBSetTime:class{
  func didSetTime(time :DayPartType)
}