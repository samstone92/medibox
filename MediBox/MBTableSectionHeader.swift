//
//  MBTableSectionHeader.swift
//  MediBox
//
//  Created by Sam Stone on 13/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class MBTableSectionHeader :UIView {
  
  private var _headerText : String!
  var headerText : String{
    set {
      self._headerText = newValue
    }
    get {
      return self._headerText
    }
  }

  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  override func layoutSubviews() {
    self.backgroundColor = UIColor.whiteColor()
    self.layer.addBorder(UIRectEdge.Bottom, color: UIColor.blackColor(), thickness: 0.5)
    setupHeaderText()

  }
  
  func setupHeaderText(){
    let headerLabel = UILabel.init(frame: CGRectMake(K.Dimensions.cellPadding, 0, self.frame.size.width, self.frame.size.height))
    headerLabel.font = UIFont.customFontMedium(K.FontSizes.titleFontSize)
    headerLabel.text = self._headerText
    self.addSubview(headerLabel)
  }
  
  
}