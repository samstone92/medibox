//
//  MBDaySegment.swift
//  MediBox
//
//  Created by Sam Stone on 08/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import UIKit
import Foundation

public class MBDaySegment : UIView{
  
  var points : Int?
  var titleLabel : UILabel!
  var selected : Bool = false
  
  @IBInspectable var labelText: String = ""
  
  
  override init(frame: CGRect) {
    super.init(frame: frame);
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder);
  }
  
  override public func layoutSubviews() {
    super.layoutSubviews()
    setup()
  }
  
  func setup() {
    self.layer.borderColor = UIColor.orangeColor().CGColor
    self.layer.borderWidth = 2
    setupTapGesture()
    setupTitleLabel()
    setToNotSelected()
  }
  
  public func setText(labelText : String){
    self.labelText = labelText
  }
  
  func setupTapGesture(){
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MBPointsButton.viewTapped))
    self.addGestureRecognizer(tapGesture)
  }
  
  func viewTapped(){
    UIView.animateWithDuration(0.5, delay: 0, options: [UIViewAnimationOptions.TransitionCrossDissolve, UIViewAnimationOptions.AllowUserInteraction],animations: {
      if self.selected {
        self.setToNotSelected()
        return
      }
      self.setToSelected()
      }, completion: nil)
  }
  
  func setToSelected(){
    self.titleLabel.textColor = UIColor.whiteColor()
    self.backgroundColor = UIColor.appOrangeColor()
    selected = true
  }
  
  func setToNotSelected(){
    self.backgroundColor = UIColor.clearColor()
    self.titleLabel.textColor = UIColor.appOrangeColor()
    selected = false
  }
  
  func setupTitleLabel() {
    titleLabel = UILabel.init(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
    titleLabel.text = labelText
    titleLabel.textAlignment = NSTextAlignment.Center
    titleLabel.textColor = UIColor.whiteColor()
    self.addSubview(titleLabel)
  }
  
}