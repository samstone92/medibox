//
//  MBConstants.swift
//  MediBox
//
//  Created by Sam Stone on 13/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

struct K {
  struct Dimensions {
    static let cellPadding :CGFloat =  30.0
  }
  
  struct FontSizes{
    static let titleFontSize :CGFloat = 23.0
  }
  
  struct UserDefaultKeys{
    static let UUID :String = "UUID"
    static let morning :String = "morning"
    static let afternoon :String = "afternoon"
    static let evening :String = "evening"
    static let night :String = "night"
    static let tokenString :String = "tokenString"
    static let email :String = "email"
    static let createDate :String = "createDate"
    static let id :String = "_id"
    static let fullName :String = "fullName"
  }
}