//
//  MBPointsButtonTest.swift
//  MediBox
//
//  Created by Sam Stone on 03/08/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

public class MBPointsButton : UIView{
  
  var points : Int?
  var titleLabel : UILabel!
  var takenLabel : UILabel!
  var taken : Bool = false
  private var _labelText :String!
  @IBInspectable var labelText: String {
    set{
      self._labelText = newValue
      self.titleLabel?.text = self._labelText
    }
    get{
      return self._labelText!
    }
  }
  
  private var _medication :MBMedicationTaken!
  var medication: MBMedicationTaken {
    set{
      self._medication = newValue
      if self._medication.taken == false{
        setNotTaken()
      } else {
        setTaken()
      }
    }
    get{
      return self._medication!
    }
  }

  override init(frame: CGRect) {
    super.init(frame: frame);
    setup()

  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder);
    setup()
  }
  
  override public func layoutSubviews() {
    setupFrames()
  }
  
  func setupFrames(){
    self.titleLabel.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
    self.takenLabel.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)
  }

  func setup() {
    self.backgroundColor = UIColor.appOrangeColor()
    setupTapGesture()
    setupTitleLabel()
    setupTakenLabel()
  }
  
  func setupTapGesture(){
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MBPointsButton.viewTapped(_:)))
    self.addGestureRecognizer(tapGesture)
  }
  
  func viewTapped(sender: AnyObject){
    UIView.animateWithDuration(0.5) { 
      if self.taken {
        self.setNotTaken()
        return
      }
      self.setTaken()
      self._medication.taken = self.taken
      self.updateMedication()
    }
  }
  
  func setNotTaken(){
    self.backgroundColor = UIColor.orangeColor()
    self.takenLabel.alpha = 0
    self.titleLabel.alpha = 1
    self.taken = false
  }
  
  func setTaken(){
    self.backgroundColor = UIColor.grayColor()
    self.takenLabel.alpha = 1
    self.titleLabel.alpha = 0
    self.taken = true
  }
  
  func updateMedication(){
    MBMedicationService.putMedication(self._medication.id, parameters: self._medication.toJSON()) { (completion:Bool) in
      print("done")
    }
  }
  
  func setupTitleLabel() {
    titleLabel = UILabel.init(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
    titleLabel.textAlignment = NSTextAlignment.Center
    titleLabel.textColor = UIColor.whiteColor()
    titleLabel.font = UIFont(name: "Ubuntu", size: 16)
    titleLabel.alpha = 1
    self.addSubview(titleLabel)
  }
  
  func setupTakenLabel(){
    takenLabel = UILabel.init(frame: CGRectMake(0, 0, self.frame.size.width, self.frame.size.height))
    takenLabel.text = "Taken"
    takenLabel.textAlignment = NSTextAlignment.Center
    takenLabel.textColor = UIColor.whiteColor()
    titleLabel.font = UIFont(name: "Ubuntu", size: 16);
    takenLabel.alpha = 0
    self.addSubview(takenLabel)

  }

}

